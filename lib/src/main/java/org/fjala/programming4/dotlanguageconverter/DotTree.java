package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;

public class DotTree implements DotStmt {

    private static final String DIGRAPH = "digraph";
    private static final String GRAPH = "graph";
    private static final String DEFAULT_NAME = "tree";
    private static final String FIST_LINE_FORMAT = "%s %s {";
    private static final String CLOSE_BRACE = "}";

    private final DotStmtList dotStmtList;
    private final String graphType;
    private String treeName;

    public DotTree(DotStmtList dotStmtList, boolean isDirectedGraph) {
        this(DEFAULT_NAME, dotStmtList, isDirectedGraph);
    }

    public DotTree(String treeName, DotStmtList dotStmtList, boolean isDirectedGraph) {
        this.treeName = DotId.correctStrId(treeName);
        this.dotStmtList = dotStmtList;
        this.graphType = isDirectedGraph ? DIGRAPH : GRAPH;
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        var currentGv = new LinkedList<String>();

        String firstLine = String.format(FIST_LINE_FORMAT, graphType, treeName);
        var stmtListGv = dotFormatter.addIndentation(dotStmtList.toGv(dotFormatter));

        currentGv.add(firstLine);
        currentGv.addAll(stmtListGv);
        currentGv.add(CLOSE_BRACE);

        return currentGv;
    }

    public String getName() {
        return treeName;
    }

    public void setName(String treeName) {
        this.treeName = DotId.correctStrId(treeName);
    }
}
