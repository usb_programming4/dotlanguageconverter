package org.fjala.programming4.dotlanguageconverter;

public enum DotOutputFormat {
    PNG("png", ".png", "-Tpng"),
    SVG("svg", ".svg", "-Tsvg"),
    JPEG("jpeg", ".jpeg", "-Tjpeg"),
    PDF("pdf", ".pdf", "-Tpdf");

    private final String displayName;
    private final String extension;
    private final String flag;

    DotOutputFormat(String displayName, String extension, String flag) {
        this.displayName = displayName;
        this.extension = extension;
        this.flag = flag;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getExtension() {
        return extension;
    }

    public String getFlag() {
        return flag;
    }
}
