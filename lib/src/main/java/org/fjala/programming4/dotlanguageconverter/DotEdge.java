package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;
import java.util.List;

public class DotEdge<T> implements DotStmt {

    private static final String DOT_EDGE_FORMAT = "%s %s %s";
    private static final String ATTR_LIST_FORMAT = " %s";

    private final T from;
    private final T to;
    private final String gv;
    private final DotAttrList attributes;

    public DotEdge(T from, T to) {
        this(from, to, false);
    }

    public DotEdge(T from, T to, boolean isDirectedGraph) {
        this(from, to, isDirectedGraph, null);
    }

    public DotEdge(T from, T to, boolean isDirectedGraph, DotAttrList attributes) {
        this.from = from;
        this.to = to;
        DotEdgeOp op = isDirectedGraph ? DotEdgeOp.DIRECTED_GRAPH : DotEdgeOp.UNDIRECTED_GRAPH;
        this.gv = String.format(DOT_EDGE_FORMAT, from, op, to);
        this.attributes = attributes;
    }

    public T getFrom() {
        return from;
    }

    public T getTo() {
        return to;
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        String result = this.gv;
        if (attributes != null) {
            result += String.format(ATTR_LIST_FORMAT, attributes);
        }
        return new LinkedList<>(List.of(result));
    }
}
