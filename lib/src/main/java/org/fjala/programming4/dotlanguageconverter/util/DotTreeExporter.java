package org.fjala.programming4.dotlanguageconverter.util;

import org.fjala.programming4.dotlanguageconverter.DotOutputFormat;
import org.fjala.programming4.dotlanguageconverter.DotTree;
import org.fjala.programming4.dotlanguageconverter.Serializer;

import java.io.File;
import java.io.IOException;

public class DotTreeExporter<T> {

    private final Serializer<T, ? extends DotTree> treeSerializer;
    private final DotOutputFormat outputFormat;
    private final File directory;
    private boolean isDotFile;

    public DotTreeExporter(Serializer<T, ? extends DotTree> treeSerializer, File directory, DotOutputFormat outputFormat) {
        this.treeSerializer = treeSerializer;
        this.outputFormat = outputFormat;
        this.directory = directory;
    }

    public void setDotFile(boolean dotFile) {
        isDotFile = dotFile;
    }

    public void export(T tree, String treeName) throws IOException {
        DotTree dotTree = treeSerializer.apply(tree);
        dotTree.setName(treeName);

        GvFileWriter.exportDotGraph(dotTree, directory, isDotFile, outputFormat);
    }
}
