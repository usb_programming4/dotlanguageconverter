package org.fjala.programming4.dotlanguageconverter.util;

import org.fjala.programming4.dotlanguageconverter.DotFormatter;
import org.fjala.programming4.dotlanguageconverter.DotOutputFormat;
import org.fjala.programming4.dotlanguageconverter.DotTree;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GvFileWriter {

    /**
     * The first string represents the flag (ex: -Tpng)
     * The second string represents the .gv file path (ex: someGraph.gv)
     * The third string represents the output file path (ex: someGraph.png)
     */
    private static final String COMMAND_FORMAT = "dot %s %s -o %s";
    private static final String GV_EXTENSION = ".gv";
    private static final String DOT_EXTENSION = ".dot";
    private static final String NON_WORD_CHARACTER = "[^a-zA-Z]";
    private static final String EMPTY_STRING = "";
    private static final String UNNAMED = "unnamed";

    private static DotFormatter formatter = DotFormatter.PRETTY_FORMATTER;

    static String normalizeFileName(String fileName) {
        String result = fileName.replaceAll(NON_WORD_CHARACTER, EMPTY_STRING);
        return result.equals(EMPTY_STRING) ? UNNAMED : result;
    }

    public static void writeFile(File out, String content) throws IOException {
        Files.writeString(Paths.get(out.toURI()), content);
    }

    public static void exportDotGraph(DotTree dotGraph, File directory, boolean dotFile, DotOutputFormat outputFormat)
            throws IOException {
        String fileName = normalizeFileName(dotGraph.getName());
        String extension = dotFile ? DOT_EXTENSION : GV_EXTENSION;
        File gvFile = new File(directory, fileName + extension);
        File outputFile = new File(directory, fileName + outputFormat.getExtension());

        String gvContent = dotGraph.toGvStr(formatter);

        GvFileWriter.writeFile(gvFile, gvContent);
        GvFileWriter.writeOutputFormat(gvFile, outputFile, outputFormat);
    }

    private static void writeOutputFormat(File gvFile, File outputFile, DotOutputFormat outputFormat) throws IOException {
        String command = String.format(COMMAND_FORMAT,
                outputFormat.getFlag(), gvFile.getAbsolutePath(), outputFile.getAbsolutePath());
        Runtime.getRuntime().exec(command);
    }

    public static void setFormatter(DotFormatter formatter) {
        if (formatter != null) {
            GvFileWriter.formatter = formatter;
        }
    }
}
