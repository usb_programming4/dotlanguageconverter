package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;
import java.util.stream.Collectors;

public class DotFormatter {

    public static final DotFormatter PRETTY_FORMATTER = new DotFormatter(Indentation.FOUR_SPACE_INDENTATION, true);
    public static final DotFormatter IN_LINE_FORMATTER = new DotFormatter(null,  false);

    private final Indentation indentation;
    private final boolean useLineBreak;

    public DotFormatter(Indentation indentation, boolean useLineBreak) {
        this.indentation = indentation;
        this.useLineBreak = useLineBreak;
    }

    public Indentation getIndentation() {
        return indentation;
    }

    public char lineBreak() {
        return useLineBreak ? '\n' : ' ';
    }

    public LinkedList<String> addIndentation(LinkedList<String> toIndent) {
        if (indentation == null) {
            return toIndent;
        }

        return toIndent.stream()
                .map(indentation::apply)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
