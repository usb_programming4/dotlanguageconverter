package org.fjala.programming4.dotlanguageconverter;

import java.util.function.Function;

public interface Serializer<T, R extends DotStmt> extends Function<T, R> {
}
