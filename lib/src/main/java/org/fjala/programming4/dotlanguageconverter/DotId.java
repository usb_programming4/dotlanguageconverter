package org.fjala.programming4.dotlanguageconverter;

public class DotId {

    private static final String QUOTE = "\"";
    private static final String EMPTY_STRING = "";

    private DotId() {}

    public static String correctStrId(String strId) {
        String result = strId;
        if (result.contains(QUOTE)) {
            result = result.replace(QUOTE, EMPTY_STRING);
        }
        if (result.contains(" ")) {
            return QUOTE + result + QUOTE;
        }
        return strId;
    }
}
