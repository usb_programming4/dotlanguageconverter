package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;
import java.util.List;

public class DotAttr implements DotStmt {

    private static final String SINGLE_ATTR_FORMAT = "%s = \"%s\"";
    private static final String NEXT_FORMAT = ", %s";

    private final String id;
    private final String value;
    private DotAttr next;

    public DotAttr(String id, String value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        return new LinkedList<>(List.of(this.toString()));
    }

    public void setNext(DotAttr next) {
        this.next = next;
    }

    @Override
    public String toString() {
        String result = String.format(SINGLE_ATTR_FORMAT, id, value);
        if (next != null){
            result += String.format(NEXT_FORMAT, next);
        }
        return result;
    }
}
