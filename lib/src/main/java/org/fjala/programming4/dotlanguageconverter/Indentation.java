package org.fjala.programming4.dotlanguageconverter;

public class Indentation {

    public static final Indentation FOUR_SPACE_INDENTATION = new Indentation(false, 4);
    public static final Indentation TAB_INDENTATION = new Indentation(true, 4);

    private static final String TAB = "\t";
    private static final String SPACE = " ";

    private final boolean useTabChar;
    private final String spaceIndentation;

    public Indentation(boolean useTabChar, int indentSize) {
        this.useTabChar = useTabChar;
        this.spaceIndentation = SPACE.repeat(indentSize);
    }

    public String apply(String input) {
        String indent = useTabChar ? TAB : spaceIndentation;
        return indent + input;
    }
}
