package org.fjala.programming4.dotlanguageconverter;

public enum DotEdgeOp {
    DIRECTED_GRAPH("->"),
    UNDIRECTED_GRAPH("--");

    private final String gv;

    DotEdgeOp(String gv) {
        this.gv = gv;
    }

    public String toGv() {
        return gv;
    }

    @Override
    public String toString() {
        return toGv();
    }
}
