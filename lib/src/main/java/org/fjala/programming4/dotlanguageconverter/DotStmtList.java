package org.fjala.programming4.dotlanguageconverter;

import java.util.ArrayList;
import java.util.LinkedList;

public class DotStmtList extends ArrayList<DotStmt> implements DotStmt {

    private static final char SEMICOLON = ';';

    private static void addLastSemicolon(LinkedList<String> gvList) {
        var newLast = gvList.getLast() + SEMICOLON;
        gvList.set(gvList.size() - 1, newLast);
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        var stmtList = new LinkedList<String>();
        this.forEach(dotStmt -> {
            var gvList = dotStmt.toGv(dotFormatter);
            addLastSemicolon(gvList);
            stmtList.addAll(gvList);
        });
        return stmtList;
    }
}
