package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;

public class DotSubgraph implements DotStmt {

    private static final String FIST_LINE_FORMAT = "subgraph %s {";
    private static final String CLOSE_BRACE = "}";
    private static final String EMPTY = "";

    private String id;
    private final DotStmtList dotStmtList;

    public DotSubgraph(DotStmtList dotStmtList) {
        this(EMPTY, dotStmtList);
    }

    public DotSubgraph(String id, DotStmtList dotStmtList) {
        this.id = id;
        this.dotStmtList = dotStmtList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        String firstLine = String.format(FIST_LINE_FORMAT, id);
        LinkedList<String> stmtListGv = dotFormatter.addIndentation(dotStmtList.toGv(dotFormatter));

        stmtListGv.addFirst(firstLine);
        stmtListGv.addLast(CLOSE_BRACE);

        return stmtListGv;
    }
}
