package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;
import java.util.List;

public class DotNodeStmt implements DotStmt {

    private static final String SPACE = " ";

    private final String id;
    private final DotAttrList attrList;

    public DotNodeStmt(String id) {
        this(id, null);
    }

    public DotNodeStmt(String id, DotAttrList attrList) {
        this.id = id;
        this.attrList = attrList;
    }

    public String getId() {
        return id;
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        return new LinkedList<>(List.of(this.toString()));
    }

    @Override
    public String toString() {
        String result = id;
        if (attrList != null) {
            result = result + SPACE + attrList;
        }
        return result;
    }
}
