package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;

public interface DotStmt {

    /**
     * Creates an array of Strings where each element represents one line for the graphviz file.
     * @return array of Strings
     */
    LinkedList<String> toGv(DotFormatter dotFormatter);

    default String toGvStr(DotFormatter dotFormatter) {
        StringBuilder builder = new StringBuilder();
        var gvList = this.toGv(dotFormatter);
        gvList.forEach(stmt -> builder.append(stmt).append(dotFormatter.lineBreak()));
        return builder.toString();
    }
}
