package org.fjala.programming4.dotlanguageconverter;

import java.util.LinkedList;
import java.util.List;

public class DotAttrList implements DotStmt {

    private static final String ATTR_LIST_FORMAT = "[ %s ]";

    private final DotAttr dotAttr;

    public DotAttrList(DotAttr dotAttr) {
        this.dotAttr = dotAttr;
    }

    public static DotAttrList of(DotAttr... dotAttrs) {
        for (int i = 0; i < dotAttrs.length - 1; i++) {
            dotAttrs[i].setNext(dotAttrs[i + 1]);
        }
        return new DotAttrList(dotAttrs[0]);
    }

    @Override
    public LinkedList<String> toGv(DotFormatter dotFormatter) {
        return new LinkedList<>(List.of(this.toString()));
    }

    @Override
    public String toString() {
        return String.format(ATTR_LIST_FORMAT, dotAttr);
    }
}
