package org.fjala.programming4.dotlanguageconverter.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GvFileWriterTest {

    @Test
    public void normalizeFileNameShouldRemoveWhiteSpaces() {
        String name = "complete tree";
        String expected = "completetree";

        String actual = GvFileWriter.normalizeFileName(name);

        assertEquals(expected, actual);
    }

    @Test
    public void normalizeFileNameShouldRemoveSpecialCharacters() {
        String name = "tree+-*";
        String expected = "tree";

        String actual = GvFileWriter.normalizeFileName(name);

        assertEquals(expected, actual);
    }

    @Test
    public void normalizeFileNameShouldRemoveNumbers() {
        String name = "tree3 number 445";
        String expected = "treenumber";

        String actual = GvFileWriter.normalizeFileName(name);

        assertEquals(expected, actual);
    }

    @Test
    public void normalizeFileNameShouldReturnTheText_unnamed_IfTheResultBecomesEmpty() {
        String name = "+-*/    445";
        String expected = "unnamed";

        String actual = GvFileWriter.normalizeFileName(name);

        assertEquals(expected, actual);
    }
}
